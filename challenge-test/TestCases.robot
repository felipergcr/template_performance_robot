*** Settings ***
# Resource         CT01.robot    
Resource         CT02.robot    
Suite Teardown   Fechar Navegador

*** Test Cases ***
# Cenário 01: Pesquisar por produto existentes
#     Dado que esteja na tela HOME da pagina da pratica de automação
#     Quando pesquisar pela palavra valida "${text}"
#     Então a postagem "${text}" deve ser listada no resultado da pesquisa

Cenário 02: Pesquisa por produtos não existentes
    Dado que esteja na tela HOME da pagina da pratica
    Quando pesquisar pela palavra invalida "${text}"
    Então deve ser exibida uma mensagem que não foi encontrado o "${text}"



