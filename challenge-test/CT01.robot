*** Settings ***
Documentation     Pesquisar por produto existentes.

Library           Selenium2Library

*** Variables ***
${browser}              Firefox
${url}                  http://automationpractice.com/index.php?
${waitScreenHome}       xpath=.//*[@id='header_logo'] 
${inputSearch}          xpath=.//*[@id='search_query_top']
${clickButtonPesquisa}  xpath=.//*[@class='btn btn-default button-search']
${clickLinkBlouse}      xpath=.//*[@class='product-image-container']


${waitProductBlouse}    xpath=.//*[@id='image-block']
${clickPhotos}          xpath=.//*[@class='span_link no-print'] 
 
${waitEnlargedPhoto}    xpath=.//*[@class='child']
${text}                 'Blouse'
${DELAY}                0

 
*** Keywords ***
#### DADO
Dado que esteja na tela HOME da pagina da pratica de automação
   Acessar a pagina home do site my store

#### QUANDO
Quando pesquisar pela palavra valida "${text}"
    Pesquisar o nome do produto valido "${text}"
    Clicar no post encontrado

#### ENTÂO
Então a postagem "${text}" deve ser listada no resultado da pesquisa
    Verificar tela da postagem 

Acessar a pagina home do site my store
    Open Browser                     ${url}  ${browser}
    Wait Until Element Is Visible    ${waitScreenHome}
    Title Should Be                  My Store

Pesquisar o nome do produto valido "${text}"
    Input Text                      ${inputSearch}    ${text}
    Click Element                   ${clickButtonPesquisa}
    Wait Until Element Is Visible   ${clickLinkBlouse}

Clicar no post encontrado
    Click Element                   ${clickLinkBlouse}

Verificar tela da postagem    
    Wait Until Element Is Visible   ${waitProductBlouse}
    Click Element                   ${clickPhotos}
    Wait Until Element Is Visible   ${waitEnlargedPhoto}

Fechar Navegador
    Close Browser  

