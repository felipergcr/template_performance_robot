*** Settings ***
Documentation     Pesquisar por produto não existentes.

Library           Selenium2Library

*** Variables ***
${browser}              Firefox
${url}                  http://automationpractice.com/index.php?
${waitScreenHome}       xpath=.//*[@id='header_logo'] 
${inputSearch}          xpath=.//*[@id='search_query_top']
${clickButtonPesquisa}  xpath=.//*[@class='btn btn-default button-search']

${messageError}         xpath=.//*[@class="alert alert-warning"]
 
${text}                 'ProdutoNãoExiste'
${DELAY}                0

*** Keywords ***
#### DADO
Dado que esteja na tela HOME da pagina da pratica
   Acessar a pagina home do site

#### QUANDO
Quando pesquisar pela palavra invalida "${text}"
    Pesquisar o nome do produto invalido "${text}" 

#### ENTÂO
Então deve ser exibida uma mensagem que não foi encontrado o "${text}" 
    Verificar mensagem de nenhum resultado para "${text}"  

Acessar a pagina home do site
    Open Browser                     ${url}  ${browser}
    Wait Until Element Is Visible    ${waitScreenHome}
    Title Should Be                  My Store

Pesquisar o nome do produto invalido "${text}"
    Input Text                      ${inputSearch}    ${text}
    Click Element                   ${clickButtonPesquisa}

Verificar mensagem de nenhum resultado para "${text}"    
    Wait Until Element Is Visible   ${messageError}

Fechar Navegador
    Close Browser  